package org.softeg.slartus.forpdaapi;

import android.net.Uri;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.softeg.slartus.forpdaapi.classes.ForumsData;
import org.softeg.slartus.forpdacommon.NotReportException;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * User: slinkin
 * Date: 08.06.12
 * Time: 13:41
 */
public class Forums extends ArrayList<Forum> {
    /**
     * Загрузка дерева разделов форума
     *
     * @param httpClient
     * @return
     * @throws Exception
     */
    public static ForumsData loadForums(IHttpClient httpClient) throws Exception, NotReportException {
        ForumsData res = new ForumsData();
        Forum mainForum = new Forum("-1", "4PDA");

        String pageBody = httpClient.performGet("http://4pda.ru/forum/index.php?act=idx");
        Document doc = Jsoup.parse(pageBody);

        for (Element catElement : doc.select("div.cat_name")) {
            Element el = catElement.select("a[href~=showforum=\\d+]").first();
            if (el == null) continue;
            Uri uri = Uri.parse(el.attr("href"));
            Forum forum = new Forum(uri.getQueryParameter("showforum"), el.text());

            forum.setHasTopics(false);
            forum.setDescription(null);
            forum.setParent(mainForum);
            res.getItems().add(forum);

        }


        return res;
    }

    public static ForumsData loadCategoryForums(IHttpClient httpClient ) throws Exception, NotReportException {
        ForumsData res = new ForumsData();
        Forum mainForum = new Forum("-1", "4PDA");

        String pageBody = httpClient.performGet("http://4pda.ru/forum/index.php?act=idx");
        Document doc = Jsoup.parse(pageBody);

        for (Element catElement : doc.select("div.cat_name")) {
            Element el = catElement.select("a[href~=showforum=\\d+]").first();
            if (el == null) continue;
            Uri uri = Uri.parse(el.attr("href"));
            Forum forum = new Forum(uri.getQueryParameter("showforum"), el.text());

            forum.setHasTopics(false);
            forum.setDescription(null);
            forum.setParent(mainForum);
            res.getItems().add(forum);

        }


        return res;
    }

    public static void markAllAsRead(IHttpClient httpClient) throws Throwable {
        httpClient.performGet("http://4pda.ru/forum/index.php?act=Login&CODE=05");
    }

    public static void markForumAsRead(IHttpClient httpClient, CharSequence forumId) throws Throwable {

        List<NameValuePair> qparams = new ArrayList<>();
        qparams.add(new BasicNameValuePair("act", "login"));
        qparams.add(new BasicNameValuePair("CODE", "04"));
        qparams.add(new BasicNameValuePair("f", forumId.toString()));
        qparams.add(new BasicNameValuePair("fromforum", forumId.toString()));


        URI uri = URIUtils.createURI("http", "4pda.ru", -1, "/forum/index.php",
                URLEncodedUtils.format(qparams, "UTF-8"), null);

        httpClient.performGet(uri.toString());
    }
}
