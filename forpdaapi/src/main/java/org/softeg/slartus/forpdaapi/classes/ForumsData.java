package org.softeg.slartus.forpdaapi.classes;

import org.softeg.slartus.forpdaapi.ICatalogItem;
import org.softeg.slartus.forpdaapi.IListItem;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by slartus on 14.02.2015.
 */
public class ForumsData implements Serializable {
    private int pagesCount = 1;
    private int currentPage = 1;
    private Throwable error=null;

    private ArrayList<ICatalogItem> items=new ArrayList<ICatalogItem>();

    public ArrayList<ICatalogItem> getItems() {
        return items;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable ex) {
        this.error = ex;
    }
}
